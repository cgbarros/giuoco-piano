\version "2.19.82"

\score {
\new StaffGroup <<
  \new Dynamics { \global }
  \new Staff <<
    \clef "G_8"
    \new Voice { \vlaoUm }
    \new Voice { \vlaoDois }
  >>
>>
}
\markup {
  \fill-line { "" "São Paulo, 1° de Agosto de 2017"  }
}