\version "2.19.58"

\include "./config/lv.ily"
\include "./config/lvMaior.ily"
\include "./config/beam-breakable.ily"
\include "./config/alternative-dynamics.ly"
\include "./config/accidentalStyle-forget.ily"
\include "./config/tupletFrac.ily"
\include "./config/tempoCa.ly"
\include "./config/tupletFeather.ily"
\include "./config/titling-alternative.ly"

#(set-global-staff-size 19.5)
\include "fonts-stylesheet/mtf-arnold.ily"

\header {
  title = "Giuoco Piano"
  composer = "Caio Giovaneti de Barros"
  instrument = "para violão"
  tagline = ""
  dedication = "ao meu pai, se divertindo"
}


%%%%%
% Mov I
%%%%%

\include "1-variables.ly"
\bookpart {
  \header {
    piece = \markup { \fill-line { \null \fontsize #5 \bold I  \null} }
  }
  \include "1-music.ly"
}

%%%%%
% Mov II
%%%%%

andamento = \tempoCa "Lentamente e con curiosità" 8 115

global = {
  \tempo \andamento
  \time 6/8 s8*6 | %1
  \time 5/8 s8*5 | %2
  \time 7/8 s8*7 | %3
  \time 6/8 s8*6 | %4
  \time 8/8 s8*8 | %5
}
vlaoUm = \relative {
  <g' a>8[\lv \p r8 e']\open \lv r8 r8 r8 | %1
  <cis,, d''>8-. r8 r8 r8 r8 | %2
  <b' c>8[\mf <g fis'>]-. r8 r8 r8 r8 r8 | %3
  <f, e'''\open>8\lv \p r8 r r r r | %4
  <cis'' dis e>8[\< d, b <c d>]\f r8 r r r
}
vlaoDois = {}
\bookpart {
  \header {
  title = ""
  composer = ""
  instrument = ""
  tagline = ""
  dedication = ""
  piece = \markup { \fill-line { \null \fontsize #5 \bold II  \null} }
  }
  \score {
\new StaffGroup <<
  \new Dynamics { \global }
  \new Staff <<
    \clef "G_8"
    \new Voice { \vlaoUm }
    \new Voice { \vlaoDois }
  >>
>>
}
}