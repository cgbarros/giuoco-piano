\version "2.19.82"

andamento = \tempoCa "Allegretto giocoso" 8 230

global = {
  \tempo \andamento
  \time 9/8 s8*9 | %1 
  \time 8/8 s8*8 | %2
  s8*8 | %3
  \time 17/8 s8*17 | %4
  \time 4/8 s8*4 | %5
  \time 10/8 s8*10 | %6
  \time 18/8 s8*18 | %7
  \time 17/8 s8*17 | %8
  \time 7/8 s8*7 | %9
  \time 14/8 s8*14 | %10
  \time 11/8 s8*11 | %11
  \time 9/8 s8*9 | %12
  s8*9 | %13
  \time 6/8 s8*6 | %14
  \time 5/8 s8*5 | %15
  \time 9/8 s8*9 | %16
  \time 11/8 s8*11 | %17
  \time 3/8 s8*3 | %18
  \time 7/16 s16*7 | %19
  \time 2/8 s8*2 | %20
  s8*2 | %21
  s8*2 | %22
  \time 3/8 s8*3 | %23
  \time 7/16 s16*7 | %24
  \time 6/8 s8*6 | %25
  \time 12/8 s8*12 | %26
  \time 8/8 s8*8 | %27
  \time 11/8 s8*11 | %28
  \time 3/8 s8*3 | %29
  \time 5/8 s8*5 | %30
  \time 4/8 s8*4 | %31
  \time 2/8 s8*2 | %32
  s8*2 | %33
  \time 8/8 s8*8 | %34
  \time 10/8 s8*10 | %35
  \time 2/8 s8*2 | %36
  \time 7/8 s8*7 | %37
  \time 7/16 s16*7 | %38
  \time 5/8 s8*5 | %39
  \time 3/8 s8*3 | %40
  \time 8/8 s8*8 | %41
  \time 2/8 s8*2 | %42
  \time 6/8 s8*6 | %43
  \time 9/8 s8*9 | %44
  \time 7/8 s8*7 | %45
  \time 3/8 s8*3 | %46
  \time 5/8 s8*5 | %47
  \time 8/8 s8*8 | %48
  \time 5/8 s8*5 | %49
  \time 4/8 s8*4 | %50
  s8*4 | %51
  \time 3/8 s8*3 | %52
  \time 4/8 s8*4 | %53
  \time 9/8 s8*9 | %54
  \time 6/8 s8*6 | %55
  \time 7/8 s8*7 | %56
  s8*7 | %57
  s8*7 | %58
  \time 11/8 s8*11 | %59
  \time 3/8 s8*3 | %60
  \time 7/8 s8*7 | %61
  \time 11/8 s8*11 | %62
  \time 3/8 s8*3 | %63
  \time 4/8 s8*4 | %64
  \time 5/8 s8*5 | %65
  \time 1/8 s8 \bar "|." | %66
}


vlaoUm = \relative {
  \oneVoice
  a8[\lv \mp b\lv dis,\lv e,\lv b''\lv fis']\lv r r r | %1
  b,8[ fis' <eis,, ais>\f q fis''\mp b, cis,] r | %2
  cis8[\> d aes'' bes\< g, b,!]\! r r | %3
  <aes'' bes>8[\p \< e,, <ges'' aes ees'>\lv \arpeggio \f \> d, d \tuplet 3/2 { ees\mp f' b, } e'^o \p \< e^o e^o b,\lv d,\lv \f cis\lv d]\lv r r r | %4
  b'8[\p c'] r r | %5
  cis,,[8\mf d \tuplet 3/2 { g, d' a'' } <g, fis' a>\sffz \arpeggio fis'\mf e fis] r | %6
  cis,8[\mf d cis'' e, fis g, a cis'-> b, a bes d e b, a g' f]\lv r8 | %7
  f8[\mf a a gis' d, e, <d' ees> cis'' <e, f> a,, a'16\f b c,8 <g' a'>-> cis,\lv \> e''\open \lv d,]\! \open \lv r8 | %8
  d,8[\mf d e16 g fis''8\p <b,, c>\mf q q | %9
  e,,8 g' bes b!16 f'' g,,8 g d'\lv \< c\lv gis,\lv e''16\lv a,,\lv c'8\lv \f c]\lv r r | %10
  aes8[\mp g \tupletFrac 5/3 { b,\f e' fis g, a, } 
  <eis ais>\p q] r r r r | %11
  \voiceOne
  a'8[\mf b dis] r \oneVoice \repeat unfold 5 { r } | %12
  \voiceOne
  r8 e g,, \oneVoice \repeat unfold 6 { r } | %13
  \voiceOne
  b'16 e \oneVoice \repeat unfold 5 { r8 } | %14
  <ais,, gis' dis' fis>8\f \repeat unfold 4 { r } | %15
  \voiceOne
  r8 d'[ gis c]\lv \oneVoice \repeat unfold 5 { r } | %16
  \voiceOne
  g,8[\mf a f' b,] r r \oneVoice \repeat unfold 5 { r } | %17
  ees'8\p r r | %18
  \voiceOne
  f16[\ff b,,] r \oneVoice r8 r | %19
  <g, c' ees b'>8\mf r8 | %20
  d'''16 c,, r8 | %21
  cis'16 dis' r8 | %22
  d,8\lv r r | %23
  \voiceOne
  <ees f>16[ g,] r16 \oneVoice r8 r | %24
  <e, aes' g' f'>8\arpeggio \pp \repeat unfold 5 { r } | %25
  \voiceOne
  r8 r e''[ bes' bes d,]\lv \oneVoice \repeat unfold 6 { r } | %26
  <fis,, d'  d' bes'>8\pp \repeat unfold 7 { r } | %27
  \voiceOne
  \tuplet 3/2 { b' e'\open b, } r \oneVoice \repeat unfold 8 { r } | %28
  <e,, a d ges' aes ees'>16\arpeggio\ff \arpeggioArrowDown q\arpeggio r8 r8 | %29
  \arpeggioNormal \voiceOne
  a''8\mf gis16 gis r8 \oneVoice r r | %30
  \voiceOne
  \tuplet 3/2 { r8 <c, f>[ e <c f>] s4 } | %31
  \oneVoice
  e,,8\psub r8 | %32
  f'''8\open \lv r8 | %33
  \tupletFeather #RIGHT #"4" 5/4 { g,16[\< c, d, gis' cis,] } fis,,8[\mf fis g g] r r | %34
  \tupletFeather #RIGHT #"2" 9/8 { f''16\ff \> ais, b c, c ais' f, b' fis'\p } \stemUp c'8[\f <a,, ees' g d' e! c'>\p \arpeggio  c''\f <a,, ees' g d' e! c'>]\p \arpeggio \lv  r8 r | %35
  \stemNeutral
  <g a'>8\mf r | %36
  \tuplet 3/2 { dis''8[\mf g,, a' } g, d'] r r r | %37
  <e, bes' d a' d e>8.\f \arpeggio r8 r | %38
  fis''[\mp g fis16 bes, c' a,,] r8 | %39
  <e a bes' fis' g c>16\ff \arpeggioArrowDown q\arpeggio r8 r | %40
  \arpeggioNormal
  \tupletFeather #LEFT #"2" 7/8 { g''16[\f \> c, d, bes' d, a'' fis]\p } \voiceOne \tuplet 3/2 { <e fis>8\f f! <e fis> } r8 \oneVoice r | %41
  <e,, a bes' fis' g c>16\ff \arpeggio \arpeggioArrowDown q\arpeggio r8 | %42
  \arpeggioNormal
  b''8[\mp e'\open \tupletFrac 5/3 { b,, e' fis g, a, } e] | %43
  <d'' e>8[\p r r <g fis'>\p r r <f,, a>\p] r r | %44
  aes'8[\< aes aes aes aes]\! r r | %45
  des,8\p r r | %46
  des8\mf des des r r | %47
  \tuplet 3/2 { e'8[\p\> f, aes' } ees\pp ees ees ees] r r | %48
  d,8[\pp d e''] r r | %49
  e8[\pp e] r r | %50
  e8[\pp e]\lv r r | %51
  e8\lv \pp r r | %52
  e8\lv \pp r r r | %53
  f,,,8[\ppp f e''' e e]\lv r r r r | %54
  e16\pp e\lv \repeat unfold 5 { r8 } | %55
  e8\pp \open \lv \repeat unfold 6 { r } | %56
  e8\pp \open \lv \repeat unfold 6 { r } | %57
  e8\pp \open \repeat unfold 6 { r } | %58
  e8\pp \open \lv \repeat unfold 10 { r } | %59
  e8[\open \< \tuplet 3/2 { a, b, dis] } | %60
  g,8[ cis' g dis <e fis> g, c, | %61
  f,8\f f f f <f bes> q q q q] r r | %62
  q8\mf r r | %63
  q8\mf r r r | %64
  f8\p r r r r | %65
  f\p | %66
}

vlaoDois = \relative {
  s8*9 | %1 
  s8*8 | %2
  s8*8 | %3
  s8*17 | %4
  s8*4 | %5
  s8*10 | %6
  s8*18 | %7
  s8*17 | %8
  s8*7 | %9
  s8*14 | %10
  s8*11 | %11
  \voiceTwo
  r8 c[ ais fis] s8*5 | %12
  f!8\ff d'' r s8*6 | %13
  des,16\pp f, s8*5 | %14
  s8*5 | %15
  g'8\pp cis, a\lv r8 s8*5 | %16
  r8 r d[ c e, bes'] s8*5 | %17
  s8*3 | %18
  r16 e,[ des''] s8*2 | %19
  s8*2 | %20
  s8*2 | %21
  s8*2 | %22
  s8*3 | %23
  r16 b,[ <cis dis>] s8*2 | %24
  s8*6 | %25
  c8[\ff fis, fis d']\lv r r s8*6 | %26
  s8*8 | %27
  \tuplet 3/2 { r8  d'[ a, d'] s4 } s8*7 | %28
  s8*3 | %29
  s8 g,8 aes16 aes s8*2 | %30
  \tuplet 3/2 { <ais, dis>8\< b <ais dis> } r8\! \oneVoice r8 | %31
  \voiceTwo
  s8*2 | %32
  s8*2 | %33
  s8*8 | %34
  s8*10 | %35
  s8*2 | %36
  s8*7 | %37
  s16*7 | %38
  s8*5 | %39
  s8*3 | %40
  s8*4 \tuplet 3/2 { r8 <a bes>[ g' <a, bes>] s4 } | %41
  s8*2 | %42
  s8*6 | %43
  s8*9 | %44
  s8*7 | %45
  s8*3 | %46
  s8*5 | %47
  s8*8 | %48
  s8*5 | %49
  s8*4 | %50
  s8*4 | %51
  s8*3 | %52
  s8*4 | %53
  s8*9 | %54
  s8*6 | %55
  s8*7 | %56
  s8*7 | %57
  s8*7 | %58
  s8*11 | %59
  s8*3 | %60
  s8*7 | %61
  s8*11 | %62
}