\version "2.19.46"

tempoCa = #(define-scheme-function (tempo figure number) 
             (string? number? number?)
             #{
               \markup { 
                 \bold { #tempo } \normal-text
                 \concat { ( \small \raise #0.5 \note #(number->string figure) #UP 
                           " ca " 
                           #(number->string number) ) } }
             #})

metronomeCa = #(define-scheme-function (figure number) 
                 (number? number?)
                 #{
                   \markup { 
                     \concat { \small \raise #0.5 \note #(number->string figure) #UP 
                               " ca " 
                               #(number->string number) } }
                 #})

%{
  Ex:

  giocoso = \tempoCa "Allegretto giocoso" 4 77

  \tempo \giocoso


  andamentoI = \metronomeCa 2 46

  \tempo \andamentoI

%}