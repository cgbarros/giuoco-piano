\version "2.19.58"

\paper {
  bookTitleMarkup = \markup {
    \override #'(baseline-skip . 4.5)
    \column {
      \fill-line { 
        \null
        \italic \fromproperty #'header:dedication 
      }
      \override #'(baseline-skip . 3.5)
      \column {
        \fill-line { 
          \null
          \huge \larger \larger \larger \larger \bold
          \fromproperty #'header:title
        }
        \fill-line {
          \large \bold \italic \fromproperty #'header:instrument
          \large \bold
          \fromproperty #'header:subtitle
        }
        \fill-line {
          \fromproperty #'header:composer
          \smaller \bold
          \fromproperty #'header:subsubtitle
        }
        \fill-line {
          \fromproperty #'header:poet
          \null
          \fromproperty #'header:date
        }
        \fill-line {
          \fromproperty #'header:arranger
          \smaller
          \fromproperty #'header:meter
        }
      }
    }
  }
}

%{ 

TESTE

\header {
  title = \title
  subtitle = \subtitle
  subsubtitle = \subsubtitle
  composer = \composer
  date = \date
  dedication = \dedication
  instrument = \instrument
  poet = \poet
  meter = \meter
  arranger = \arranger
}

\relative { c' d e f }

%}