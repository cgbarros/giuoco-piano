\version "2.19.35"

%ALTERNATIVE (TRUE) DYNAMICS

%Additions

pf = #(make-dynamic-script "pf")
sffz = #(make-dynamic-script "sffz")
ffp = #(make-dynamic-script "ffp")

%Parenthesized

openPar = #(make-dynamic-script (markup #:normal-text #:huge "(" ))
closePar = #(make-dynamic-script (markup #:normal-text #:huge ")" ))

parFf = #(make-dynamic-script (markup #:normal-text #:huge "(" #:dynamic "ff" #:normal-text #:huge ")" ))

parPp = \markup {
  \concat {
   \vcenter { \tiny "(" \dynamic pp \tiny ")" }
  }
}
parPp = #(make-dynamic-script parPp)

parPpp = \markup {
  \concat {
   \vcenter { \tiny "(" \dynamic ppp \tiny ")" }
  }
}
parPpp = #(make-dynamic-script parPpp)

%Open parenthesis

ffPar = #(make-dynamic-script (markup #:dynamic "ff" #:normal-text #:huge " (" ))
fPar = #(make-dynamic-script (markup #:dynamic "f" #:normal-text #:huge " (" ))
ppPar = #(make-dynamic-script (markup #:dynamic "pp" #:normal-text #:huge ")" ))
pppPar = #(make-dynamic-script (markup #:dynamic "ppp" #:normal-text #:huge ")" ))

%Subito

fffsub = \markup { \line { \dynamic "fff" \normal-text \italic \bold "sub." }}
fffsub = \tweak DynamicText.self-alignment-X #LEFT #(make-dynamic-script fffsub)

ffsub = \markup { \line { \dynamic "ff" \normal-text \italic \bold "sub." }}
ffsub = \tweak DynamicText.self-alignment-X #LEFT #(make-dynamic-script ffsub)

fsub = \markup { \line { \dynamic "f" \normal-text \italic \bold "sub." }}
fsub = \tweak DynamicText.self-alignment-X #LEFT #(make-dynamic-script fsub)

fpsub = \markup { \line { \dynamic "fp" \normal-text \italic \bold "sub." }}
fpsub = \tweak DynamicText.self-alignment-X #LEFT #(make-dynamic-script fpsub)

psub = \markup { \line { \dynamic "p" \normal-text \italic \bold "sub." }}
psub = \tweak DynamicText.self-alignment-X #LEFT #(make-dynamic-script psub)

ppsub = \markup { \line { \dynamic "pp" \normal-text \italic \bold "sub." }}
ppsub = \tweak DynamicText.self-alignment-X #LEFT #(make-dynamic-script ppsub)

subito = \markup { \line { \italic \bold "subito" }}
subito = \tweak DynamicText.self-alignment-X #LEFT #(make-dynamic-script subito)

sub = \markup { \line { \italic \bold "sub." }}
sub = \tweak DynamicText.self-alignment-X #LEFT #(make-dynamic-script sub)

%Meno

menof = \markup { \line { \normal-text \italic \bold "meno" \dynamic "f" }}
menof = #(make-dynamic-script menof)

%Sempre

ppsempre = \markup { \line { \dynamic "pp" \vcenter \normal-text \italic "sempre" }}
ppsempre = \tweak DynamicText.self-alignment-X #LEFT #(make-dynamic-script ppsempre)

psempre = \markup { \line { \dynamic "p" \vcenter \normal-text \italic "sempre" }}
psempre = \tweak DynamicText.self-alignment-X #LEFT #(make-dynamic-script psempre)

fsempre = \markup { \line { \dynamic "f" \vcenter \normal-text \italic "sempre" }}
fsempre = \tweak DynamicText.self-alignment-X #LEFT #(make-dynamic-script fsempre)

sempreF = \markup { \line { \vcenter \normal-text \italic "sempre" \dynamic "f" }}
sempreF = \tweak DynamicText.self-alignment-X #LEFT #(make-dynamic-script sempreF)

sempre = \markup { \line { \vcenter \normal-text \italic "sempre" }}
sempre = \tweak DynamicText.self-alignment-X #LEFT #(make-dynamic-script sempre)

%Diminuendo

ffdim = \markup { \line { \dynamic "ff" \normal-text \bold \italic "dim." }}
ffdim = \tweak DynamicText.self-alignment-X #LEFT #(make-dynamic-script ffdim)

fffdim = \markup { \line { \dynamic "fff" \normal-text \bold \italic "dim." }}
fffdim = \tweak DynamicText.self-alignment-X #LEFT #(make-dynamic-script fffdim)

%%% SPANNERS %%%

%dim bold
dim = #(make-music 'DecrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text (markup #:bold #:italic "dim." ))

pocoDim = #(make-music 'DecrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text (markup #:bold #:italic "poco" ))

aDim = #(make-music 'DecrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text (markup #:bold #:italic "a" ))

finoDim = #(make-music 'DecrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text (markup #:bold #:italic "fino" ))

alDim = #(make-music 'DecrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text (markup #:bold #:italic "al" ))

%cresc. bold
cresc = #(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text (markup #:bold #:italic "cresc." ))