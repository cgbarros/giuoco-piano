\version "2.19.64"

tupletFrac = #(define-music-function (fraction music)
                (fraction? ly:music?)
   #{
     \once \override TupletNumber.text = #tuplet-number::calc-fraction-text
     \tuplet #fraction #music
   #})

% quick tuplet with fraction number (I use it for unusual tuplets)
% \relative {
%   \tuplet 3/2 { c8 c c }
%   \fracTuplet 5/3 { c8 c c c c }
% }