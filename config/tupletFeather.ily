\version "2.19.64"

tupletFeather = #(define-music-function (grow-direction duration fraction  music)
                   (ly:dir? string? fraction? ly:music?)
                   #{
                     \once \override Beam.grow-direction = #grow-direction
                     \once \override TupletBracket.bracket-visibility = ##t
                     \once \override TupletNumber.text = \markup { \note #duration #UP }
                     \once \override Score.TupletBracket.padding = #1.8
                     \tuplet #fraction #music
                   #})

%{

% Usage:
\relative c'' { \tupletFeather #RIGHT #"4" 5/4 { c16[ c c c c c] } }

% We set a fixed padding of 1.8, although sometimes it collides, e. g.
{ \tupletFeather #RIGHT #"4." 2/3 { c'''8[ c] } }

% But this is also true for regular tuplets:
{ 
  \once \override TupletBracket.bracket-visibility = ##t
  \tuplet 2/3 { c'''8[ c] } 
}

%}