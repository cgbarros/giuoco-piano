\version "2.19.64"

\layout {
  \context {
    \Score
    \override LaissezVibrerTie.X-extent = #'(0 . 5)
    \override LaissezVibrerTie.details.note-head-gap = #-0.4
    \override LaissezVibrerTie.extra-offset = #'(0.7 . 0)
  }
}