\layout {
  \context {
    \Score
    \override Beam.breakable = ##t
  }
}

% activates breakable beams, that is, beams that extend for por than one system
% test:
% \relative {
%   c'8[ c c c c c c c | \break
%   c c c c c c c c] |
% }