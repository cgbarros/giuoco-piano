\version "2.19.58"

\relative {
 c''8[ c c c c c c c | \break
 c c c c c c c c]
}

\include "breakable.ly"

% \layout {
%   \context {
%     \Staff
%     \override Beam.breakable = ##t
%   }
% }